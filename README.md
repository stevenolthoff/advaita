# Advaita

## Steven Olthoff

## Run

Use a 12.x version of node.
`nvm use 12.18`

`npm i`

`npm run start:dev`

Navigate to `http://localhost:4200/`.

*Note*: Chrome is the only browser I tested on. A few css tweaks would likely be necessary to support other browsers.