import { Component, OnInit } from '@angular/core';
import { Disease } from 'src/app/services/diseases.service';

@Component({
  selector: 'app-diseases-panel',
  templateUrl: './diseases-panel.component.html',
  styleUrls: ['./diseases-panel.component.scss']
})
export class DiseasesPanelComponent implements OnInit {

  public selectedDisease: Disease = null;

  constructor() { }

  ngOnInit(): void {
  }

  public selectDisease(disease: Disease) {
    this.selectedDisease = disease;
  }

}
