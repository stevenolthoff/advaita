import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiseasesPanelComponent } from './diseases-panel.component';

describe('DiseasesPanelComponent', () => {
  let component: DiseasesPanelComponent;
  let fixture: ComponentFixture<DiseasesPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiseasesPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiseasesPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
