import { Injectable } from '@angular/core';
import axios from 'axios';

export interface Disease {
  name: string;
  children?: Disease[];
}

@Injectable({
  providedIn: 'root'
})
export class DiseasesService {

  constructor() { }

  public async getDiseases(): Promise<Disease> {
    const response = await axios('https://stevenolthoff-assets.s3.us-east-2.amazonaws.com/br08403.json');
    return response.data;
  }
}
