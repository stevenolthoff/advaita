import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DiseasesComponent } from './components/diseases/diseases.component';
import { DiseaseInfoComponent } from './components/disease-info/disease-info.component';
import { DiseasesPanelComponent } from './pages/diseases-panel/diseases-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    DiseasesComponent,
    DiseaseInfoComponent,
    DiseasesPanelComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
