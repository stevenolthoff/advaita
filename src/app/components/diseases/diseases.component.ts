import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DiseasesService, Disease } from '../../services/diseases.service';
import * as d3 from 'd3';

interface NodePosition {
  x0: number;
  x1: number;
  y0: number;
  y1: number;
}

interface Node extends d3.HierarchyRectangularNode<Disease> {
  current?: NodePosition;
  target?: NodePosition;
}

@Component({
  selector: 'app-diseases',
  templateUrl: './diseases.component.html',
  styleUrls: ['./diseases.component.scss']
})
export class DiseasesComponent implements OnInit {

  @Output()
  public selectDisease = new EventEmitter<Disease>();
  public selectedDisease: Disease = null;
  public diseases: Disease;
  public readonly width: number = 1000;

  private readonly numVisibleChartLayers = 2;
  private parent: d3.Selection<SVGCircleElement, d3.HierarchyRectangularNode<any>, HTMLElement, any>;
  private rootNode: d3.HierarchyRectangularNode<any>;
  private svgGroup: d3.Selection<SVGGElement, any, HTMLElement, any>;
  private nodePaths: d3.Selection<Element | d3.EnterElement | Document | Window |
    SVGPathElement, d3.HierarchyRectangularNode<any>, SVGGElement, any>;
  private arcGenerator: d3.Arc<any, NodePosition>;

  constructor(private diseasesService: DiseasesService) { }

  async ngOnInit() {
    this.diseases = await this.diseasesService.getDiseases();
    this.setupChart();
    this.setupCursorHover();
    this.setupTooltip();
  }

  public selectRootNode() {
    this.selectNode(this.rootNode);
  }

  private setupChart() {
    const radius = this.width / 6;
    this.setArcGenerator(radius);

    this.setRootNode();
    this.rootNode.each((node: Node) => {
      node.current = {
        x0: node.x0,
        x1: node.x1,
        y0: node.y0,
        y1: node.y1
      };
    });

    const colorGenerator = d3.scaleOrdinal().range(d3.quantize(d3.interpolateRainbow, this.diseases.children.length + 1));

    const svg = d3.select('#partitionSVG')
      .style('width', '100%')
      .style('height', 'auto');

    this.svgGroup = svg.append('g')
      .attr('transform', `translate(${this.width / 2},${this.width / 2})`);

    this.nodePaths = this.svgGroup.append('g')
      .selectAll('path')
      .data(this.rootNode.descendants().slice(1))
      .join('path')
      .attr('fill', (d: Node) => {
        while (d.depth > 1) {
          d = d.parent;
        }
        return colorGenerator(d.data.name) as any;
      })
      .attr('fill-opacity', (node: Node) => this.getUpdatedFillOpacity(node.current, node.children))
      .attr('d', (node: Node) => this.arcGenerator(node.current));

    this.svgGroup
      .selectAll('path')
      .on('click', (node: Node) => this.selectNode(node));

    this.parent = this.svgGroup.append('circle')
      .datum(this.rootNode)
      .attr('r', radius)
      .attr('fill', 'none')
      .attr('pointer-events', 'all')
      .style('cursor', 'pointer')
      .on('click', (node: Node) => this.selectNode(node));
  }

  private setupCursorHover() {
    this.nodePaths
      .each((node: Node, index, nodes: Element[]) => {
        if (this.nodeArcIsVisible(node.current)) {
          nodes[index].setAttribute('cursor', 'pointer');
        } else {
          nodes[index].setAttribute('cursor', 'default');
        }
      });
  }

  private setupTooltip() {
    const tooltip = d3.select('#diseases__tooltip');
    this.svgGroup
      .selectAll('path')
      .on('mouseenter', (node: Node, index, nodes: Element[]) => {
        const opacity = parseFloat(nodes[index].getAttribute('fill-opacity'));
        const nodeIsVisible = Boolean(opacity);
        if (nodeIsVisible) {
          tooltip.style('visibility', 'visible');
        }
      })
      .on('mousemove', (node: Node) => {
        const bottomPadding = 20;
        if (tooltip.style('visibility') !== 'visible') {
          return;
        }
        tooltip.html(node.data.name)
          .style('left', `${d3.event.pageX}px`)
          .style('top', `${d3.event.pageY + bottomPadding}px`);
      })
      .on('mouseleave', () => {
        tooltip.style('visibility', 'hidden');
      });
  }

  private selectNode(selectedNode: Node) {
    const selectedCenterNode = selectedNode.current.y0 === 0;
    if (!selectedCenterNode && !this.nodeArcIsVisible(selectedNode.current)) {
      return;
    }

    const selectedRootNode = selectedNode.parent === null;
    if (selectedRootNode) {
      this.selectDisease.emit(null);
    } else {
      this.selectDisease.emit(selectedNode.data);
    }

    if (!selectedNode.data.children) {
      return;
    }

    this.parent.datum(selectedNode.parent || this.rootNode);

    this.rootNode.each((node: Node) => node.target = this.getTargetPosition(node, selectedNode));

    const transition = this.svgGroup.transition()
      .duration(750)
      .on('end', () => {
        this.rootNode.each((node: Node) => node.current = node.target);
        this.setupCursorHover();
    });

    this.nodePaths.transition(transition)
      .tween('data', (node: Node) => {
        const interpolator = d3.interpolate(node.current, node.target);
        return transitionProgress => node.current = interpolator(transitionProgress);
      })
      .attr('fill-opacity', (node: Node) => this.getUpdatedFillOpacity(node.target, node.children))
      .attrTween('d', (d: Node) => () => this.arcGenerator(d.current));
  }

  private getTargetPosition(node: Node, selectedNode: Node): NodePosition {
    return {
      x0: Math.max(0, Math.min(1, this.getStartAngleProportionOfArc(node, selectedNode))) * 2 * Math.PI,
      x1: Math.max(0, Math.min(1, this.getEndAngleProportionOfArc  (node, selectedNode))) * 2 * Math.PI,
      y0: Math.max(0, node.y0 - selectedNode.depth),
      y1: Math.max(0, node.y1 - selectedNode.depth)
    };
  }

  private getStartAngleProportionOfArc(node: Node, selectedNode: Node) {
    const arcTotalDistance = selectedNode.x1 - selectedNode.x0;
    return (node.x0 - selectedNode.x0) / arcTotalDistance;
  }

  private getEndAngleProportionOfArc(node: Node, selectedNode: Node) {
    const arcTotalDistance = selectedNode.x1 - selectedNode.x0;
    return (node.x1 - selectedNode.x0) / arcTotalDistance;
  }

  private setArcGenerator(radius: number) {
    this.arcGenerator = d3.arc<NodePosition>()
      .startAngle(d => d.x0)
      .endAngle(d => d.x1)
      .innerRadius(d => d.y0 * radius)
      .outerRadius(d => Math.max(d.y0 * radius, d.y1 * radius - 1))
      .padAngle(d => Math.min((d.x1 - d.x0) / 2, 0.005))
      .padRadius(radius * 1.5);
  }

  private setRootNode() {
    const hierarchy = d3.hierarchy(this.diseases)
      .sum(d =>  d.children ? 0 : 1)
      .sort((a, b) => {
        const stringA = a.data.name.toLowerCase();
        const stringB = b.data.name.toLowerCase();
        if (stringA < stringB) {
          return -1;
        }
        if (stringB < stringA) {
          return 1;
        }
        return 0;
      });
    const partition = d3.partition()
      .size([2 * Math.PI, hierarchy.height + 1]);
    this.rootNode = partition(hierarchy);
  }

  private nodeArcIsVisible(position: NodePosition): boolean {
    const numRootLayers = 1;
    const nodeIsInVisibleRange = position.y0 >= numRootLayers &&
      position.y1 <= this.numVisibleChartLayers + numRootLayers;
    return nodeIsInVisibleRange && position.x0 < position.x1;
  }

  private getUpdatedFillOpacity(nodePosition: NodePosition, children: any[] | null): number {
    if (this.nodeArcIsVisible(nodePosition)) {
      if (children) {
        return 0.6;
      }
      return 0.4;
    }
    return 0;
  }
}
