import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-disease-info',
  templateUrl: './disease-info.component.html',
  styleUrls: ['./disease-info.component.scss']
})
export class DiseaseInfoComponent implements OnInit {

  @Input()
  diseaseName: string;

  constructor() { }

  ngOnInit(): void {
  }

}
