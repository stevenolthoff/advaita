const express = require('express');

const app = express();
app.use(express.static('./dist/advaita'));
app.get('/*', function(req, res) {
    res.sendFile('index.html', {root: 'dist/advaita/'}
  );
});
const port = process.env.PORT || 8080
app.listen(port);
console.log(`Listening on ${port}.`)